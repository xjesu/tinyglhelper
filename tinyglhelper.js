
const CONTAINERS = {
	 'UNSIGNED_BYTE': Uint8Array,
	'UNSIGNED_SHORT': Uint16Array,
	         'FLOAT': Float32Array,
	    'HALF_FLOAT': Float32Array,
	        'DOUBLE': Float64Array
};

const SETUNIFORMFUNC = {
	'uint': 'uniform1i',
	'bool': 'uniform1i',
	'vec2': 'uniform2fv',
	'vec3': 'uniform3fv',
	'vec4': 'uniform4fv',
	'mat2': 'uniformMatrix2fv',
	'mat3': 'uniformMatrix3fv',
	'mat4': 'uniformMatrix4fv',
	'float': 'uniform1f',
	'double': 'uniform1f',
	'int': 'uniform1i',
	'sampler2D': 'uniform1i',
	'samplerCube': 'uniform1i',
	'sampler2DArray': 'uniform1i'
};

const ISMATRIXFUNC = {
	'uniformMatrix2fv': true,
	'uniformMatrix3fv': true,
	'uniformMatrix4fv': true
};

const GLSL_TYPES = [
	'int',
	'float',
	'vec2',
	'vec3',
	'vec4',
	'ivec2',
	'ivec3',
	'ivec4',
	'mat2',
	'mat3',
	'mat4',
	'sampler2D',
	'sampler2DArray',
	'samplerCube'
];

const BUFFER_TYPE = {
	  'highp': 'FLOAT',
	'mediump': 'UNSIGNED_BYTE'
};

const BUFFER_CPV = {
	    'float': 1,
	   'double': 1,
	      'int': 1,
	     'uint': 1,
	     'bool': 1,
	     'vec2': 2,
	    'ivec2': 2,
	     'vec3': 3,
	    'ivec3': 3,
	     'vec4': 4,
	    'ivec4': 4
};

const IS_TEXTURE = {
	'sampler2D': true,
	'sampler2DArray': true,
	'samplerCube': true
}

const GLSL_QUALIFIERS = ['highp', 'mediump', 'lowp'];

const es300preambulaVert = `
#if __VERSION__ == 300
	#define attribute in
	#define varying out
#endif
precision mediump float;
precision mediump int;`;
const es300preambulaFrag = `
#if __VERSION__ == 300
	precision mediump sampler2DArray;
	#define varying in
	layout(location = 0) out highp vec4 pc_fragColor;
	#define gl_FragColor pc_fragColor
	#define texture2D texture
#endif
precision mediump float;
precision mediump int;`;

function parseShader(code, what) {
	var strings = code.split('---').join('').split(/[\n;{}]+/),
		n = 0,
		vars = null,
		items = {};

	for (let i = 0; i < strings.length; i++) {
		strings[i] = strings[i].trim();
		if (strings[i][0] === '#') continue;
		if (!strings[i].indexOf(what)) {
			vars = strings[i].split(/[\t\n ,;]+/);
			let qual, type, name, arrayLength;
			if (GLSL_QUALIFIERS.indexOf(vars[1]) !== -1) {
				qual = vars[1];
				type = vars[2];
				name = vars[3];
			} else {
				qual = 'highp';
				type = vars[1];
				name = vars[2];
			}
			if (GLSL_TYPES.indexOf(type) === -1) {
				console.warn('GLSL: unsupported type "' + strings[i] + '"');
				continue;
			}
			let arrLengthInd = name.indexOf('[');
			if (arrLengthInd !== -1) {
				let count = parseInt(name.substr(arrLengthInd + 1));
				if (count > 0) {
					name = name.substr(0, arrLengthInd);
					for (let j = 0; j < count; j++) {
						items[name + '[' + j + ']'] = {
							type,
							qual,
							location: -1,
							n
						};
					}
				} else {
					console.warn('Wrong array length "' + strings[i] + '"');
					continue;
				}
			} else {
				items[name] = {
					type,
					qual,
					location: -1,
					n
				};
			}
			if (IS_TEXTURE[type]) n++;
		}
	}

	return items;
}

class Buffer {
	constructor(type, componentsPerVertex, target, usage, verticiesCount, dbgName) {
		this.buffer = null;
		this.type = type || 'FLOAT';
		this.data = new CONTAINERS[this.type](verticiesCount * componentsPerVertex);
		this.cpv = componentsPerVertex || 1;``
		this.target = target || 'ARRAY_BUFFER';
		if (verticiesCount > 0) {
			this.usage = 'STATIC_DRAW';
		} else {
			this.usage = usage || 'STREAM_DRAW';
		}
		this.dbgName = dbgName;
	}
	bind(gl, shader, attributeName) {
		if (!this.buffer) {
			this.buffer = gl.createBuffer();
			this.buffer.toString = () => '"' + this.dbgName + '"';
		}
		gl.bindBuffer(gl[this.target], this.buffer);
		
		if (this.usage !== 'ELEMENT_ARRAY_BUFFER') shader.setAttribute(gl, attributeName, this.cpv, this.type);
	}
	sendData(gl) {
		if (this.data) {
			gl.bufferData(gl[this.target], this.data, gl[this.usage]);
		}
	}
	alloc(from, length) {
		if (this.usage === 'STATIC_DRAW') {
			if ((from + length) * this.cpv <= this.data.length) {
				return this.data.subarray(from * this.cpv, (from + length) * this.cpv);
			} else {
				console.error('Static buffer is too small');
				debugger;
			}
		} else {
			if ((from + length) * this.cpv >= this.data.length) {
				let newData = new CONTAINERS[this.type]((from + length) * this.cpv);
				newData.set(this.data);
				this.data = newData;
			}
			return this.data.subarray(from * this.cpv, (from + length) * this.cpv);
		}
	}
	getVertexPos(nVert) {
		return nVert * this.cpv;
	}
}

class Sampler {
	constructor(filter, wrap, image) {
		this.id = null;
		this.hash = '';
		this.wrap = wrap || 'CLAMP_TO_EDGE';
		this.filter = filter || 'LINEAR';
		this.image = image || null;
	}
	update(gl) {
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl[this.wrap]);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl[this.wrap]);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl[this.filter]);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl[this.filter]);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.image);
		this.hash = this.getHash();
	}
	getHash() {
		return this.wrap + ',' + this.filter + ',' + this.image.src;
	}
	bind(gl, shader, i) {
		if (this.image) {
			if (!this.id) this.id = gl.createTexture();
			gl.activeTexture(gl.TEXTURE0 + i);
			gl.bindTexture(gl.TEXTURE_2D, this.id);
			if (this.hash !== this.getHash()) this.update(gl);
			return shader.setUniformSlot(gl, i, i);
		}
	}
}

class ArrayTexture extends Array {
	constructor(width, height, filter, wrap, storageFormat, pixelFormat) {
		super();
		this.id = null;
		this.hash = '';
		this.width = width;
		this.height = height;
		this.filter = filter || 'LINEAR';
		this.wrap = wrap || 'CLAMP_TO_EDGE';
		this.storageFormat = storageFormat || 'RGBA8';
		this.pixelFormat = pixelFormat || 'RGBA';
	}
	update(gl) {
		gl.texStorage3D(gl.TEXTURE_2D_ARRAY, 1, gl[this.storageFormat],
			this.width, this.height, this.length);
		gl.texParameteri(gl.TEXTURE_2D_ARRAY, gl.TEXTURE_WRAP_S, gl[this.wrap]);
		gl.texParameteri(gl.TEXTURE_2D_ARRAY, gl.TEXTURE_WRAP_T, gl[this.wrap]);
		gl.texParameteri(gl.TEXTURE_2D_ARRAY, gl.TEXTURE_MIN_FILTER, gl[this.filter]);
		gl.texParameteri(gl.TEXTURE_2D_ARRAY, gl.TEXTURE_MAG_FILTER, gl[this.filter]);
		
		let i = 0;
		for (let img of this) {
			gl.texSubImage3D(gl.TEXTURE_2D_ARRAY,
				0, 0, 0,
				i++, this.width, this.height, 1,
				gl[this.pixelFormat], gl.UNSIGNED_BYTE, img);
		}
		this.hash = this.getHash();
	}
	getHash() {
		return this.width + this.wrap + this.height + this.filter + this.length;
	}
	bind(gl, shader, i) {
		if (this.length) {
			if (!this.id) this.id = gl.createTexture();
			gl.activeTexture(gl.TEXTURE0 + i);
			gl.bindTexture(gl.TEXTURE_2D_ARRAY, this.id);
			if (this.hash !== this.getHash()) this.update(gl);
			return shader.setUniformSlot(gl, i, i);
		}
	}
}

class Shader {
	constructor(fragSrc, vertSrc) {
		this.attributes = {};
		this.uniforms = {};
		this.program = null;
		this.hash = null;
		this.vertSrc = vertSrc || '';
		this.fragSrc = fragSrc || '';
	}
	update(gl) {
		let vertShader = gl.createShader(gl.VERTEX_SHADER),
			fragShader = gl.createShader(gl.FRAGMENT_SHADER);

		let genVertexShader = (varyings) => {
			let header = 'attribute vec2 position;\nuniform vec2 OFFSET;\n';
			let code = 'void main() {\n\tgl_Position = vec4(position + OFFSET, 0.0, 1.0);\n';
			for (let i in varyings) {
				header += `attribute ${varyings[i].type} ${i.substr(2)};\n`;
				header += `varying ${varyings[i].type} ${i};\n`;
				code += `\t${i} = ${i.substr(2)};\n`;
			}
			return header + code + '}';
		}

		if (!this.fragSrc) {
			console.error('No fragment shader');
			return;
		}

		if (!this.vertSrc) {
			let varyings = parseShader(this.fragSrc, 'varying');
			this.vertSrc = genVertexShader(varyings);
		}

		let fullText = this.vertSrc + '\n' + this.fragSrc;

		this.attributes = parseShader(fullText, 'attribute');
		this.uniforms = parseShader(fullText, 'uniform');

		let compile = (shader, src) => {;
			gl.shaderSource(shader, src);
			gl.compileShader(shader);
			let status = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
			if (!status) {
				src = src.split('\n');
				let mainErr = gl.getShaderInfoLog(shader);
				let errors = mainErr.split('\n').map(e => e.trim()).filter(e => e.length > 2);
				let descr = [];
				for (let err of errors) {
					try {
						let pos0 = err.indexOf('0:');
						if (pos0 === -1) throw '';
						let errText = err.substring(pos0 + '0:'.length);
						let n = parseInt(errText);
						errText = errText.substr((n + ':').length);
						let symbol = errText.split('\'')[1];
						let tmp = errText.split(':');
						tmp.shift();
						errText = tmp.join('').trim();
						let line = src[n - 1].split('//')[0].trim();
						function getAccent(line, symbol) {
							if (!symbol) return '';
							let res = '';
							while (line) {
								let spacesCount = line.indexOf(symbol);
								if (spacesCount < 0) {
									break;
								}
								for (let i = 0; i < spacesCount; i++) {
									res += ' ';
								}
								for (let i = 0; i < symbol.length; i++) {
									res += '^';
								}
								line = line.substr(spacesCount + symbol.length);
							}
							return res;
						}
						descr.push(`${errText[0].toUpperCase() + errText.substr(1)}:\n${line}\n${getAccent(line, symbol)}`);
					} catch (e) {
						descr.push(mainErr);
					}
				}
				throw 'ShaderCompileError\n\n' + descr.join('\n\n') + '\n\n';
			}
			return status;
		};

		this.hash = this.getHash();

		if (!compile(vertShader, gl.slv + es300preambulaVert + this.vertSrc)) return;
		if (!compile(fragShader, gl.slv + es300preambulaFrag + this.fragSrc)) return;

		if (this.program) gl.deleteProgram(this.program);
		this.program = gl.createProgram();

		gl.attachShader(this.program, vertShader);
		gl.attachShader(this.program, fragShader);
		gl.linkProgram(this.program);
		gl.validateProgram(this.program);

		if (!gl.getProgramParameter(this.program, gl.LINK_STATUS)) {
			console.error(gl.getProgramInfoLog(this.program));
			debugger;
		}

		for (let name in this.attributes) this.attributes[name].location = gl.getAttribLocation(this.program, name);
		for (let name in this.uniforms) this.uniforms[name].location = gl.getUniformLocation(this.program, name);
	}
	getHash() {
		return this.vertSrc + '\n' + this.fragSrc;
	}
	setUniform(gl, name, value) {
		if (this.uniforms[name]) {
			var funcName = SETUNIFORMFUNC[this.uniforms[name].type];
			if (ISMATRIXFUNC[funcName]) {
				gl[funcName](this.uniforms[name].location, false, value);
			} else {
				gl[funcName](this.uniforms[name].location, value);
			}
			return true;
		}
	}
	setUniformSlot(gl, n, value) {
		let i = 0;
		for (let name in this.uniforms) {
			if (IS_TEXTURE[this.uniforms[name].type]) {
				if (this.uniforms[name].n === n) {
					var funcName = SETUNIFORMFUNC[this.uniforms[name].type];
					gl[funcName](this.uniforms[name].location, value);
					return true;
				}
				i++;
			}
		}
	}
	setAttribute(gl, name, componentsPerVertex, type) {
		if (this.attributes[name] && this.attributes[name].location !== -1) {
			gl.vertexAttribPointer(
				this.attributes[name].location,
				componentsPerVertex,
				gl[type],
				false,
				0,
				0
			);
			gl.enableVertexAttribArray(this.attributes[name].location);
			return true;
		}
	}
	use(gl) {
		if (this.hash !== this.getHash() || !this.program) this.update(gl);
		if (this.program) {
			gl.useProgram(this.program);
			return true;
		} else {
			return false;
		}
	}
}

class Target {
	constructor(width, height, wrap, filter, needClear, saveAsPng) {
		this.needClear = needClear || true;
		this.texture = null;
		this.framebuffer = null;
		this.width = width || 64;
		this.height = height || 64;
		this.wrap = wrap || 'CLAMP_TO_EDGE';
		this.filter = filter || 'NEAREST';
		this.hash = null;
		this.saveAsPng = saveAsPng || true;
		this.dataURL = null;
	}
	getHash() {
		return this.width + this.height + this.wrap + this.filter;
	}
	updateTexture(gl) {
		this.texture = gl.createTexture();
		gl.bindTexture(gl.TEXTURE_2D, this.texture);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, this.width, this.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);

		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl[this.filter]);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl[this.filter]);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl[this.wrap]);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl[this.wrap]);
	}
	updateFramebuffer(gl) {
		this.framebuffer = gl.createFramebuffer();
		gl.bindFramebuffer(gl.FRAMEBUFFER, this.framebuffer);
		gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.texture, 0);

		if (gl.checkFramebufferStatus(gl.FRAMEBUFFER) !== gl.FRAMEBUFFER_COMPLETE) console.error(gl.checkFramebufferStatus(gl.FRAMEBUFFER));
	}
	bind(gl, shader, i) {
		gl.activeTexture(gl.TEXTURE0 + i);
		gl.bindTexture(gl.TEXTURE_2D, this.texture);
		shader.setUniformSlot(gl, i, i);
	}
	beginRenderTo(gl) {
		if (!this.texture) this.updateTexture(gl);
		if (!this.framebuffer) this.updateFramebuffer(gl);
		gl.bindFramebuffer(gl.FRAMEBUFFER, this.framebuffer);
		gl.viewport(-this.width, -this.height, this.width * 2, this.height * 2);
		if (this.needClear) gl.clear(gl.COLOR_BUFFER_BIT);
	}
	endRenderTo(gl) {
		gl.bindFramebuffer(gl.FRAMEBUFFER, null);
		gl.resetViewport();
		if (this.saveAsPng) this.dataURL = this.toDataURL(gl, 'image/png');
	}
	toDataURL(gl, type, param) {
		var data = new Uint8Array(this.width * this.height * 4),
			canvas = document.createElement('canvas'),
			ctx = canvas.getContext('2d'),
			imgdata = ctx.createImageData(this.width, this.height),
			imgdatalen = imgdata.data.length;

		canvas.width = this.width;
		canvas.height = this.height;

		gl.bindFramebuffer(gl.FRAMEBUFFER, this.framebuffer);
		gl.readPixels(0, 0, this.width, this.height, gl.RGBA, gl.UNSIGNED_BYTE, data);
		gl.bindFramebuffer(gl.FRAMEBUFFER, null);

		for(var i = 0; i < imgdatalen; i++) {
			imgdata.data[i] = data[i];
		}
		ctx.putImageData(imgdata, 0, 0);

		return canvas.toDataURL(type, param);
	}
}

class Batch extends Array {
	constructor(buffers, shader, STATIC, glEnable) {
		super();

		this.shader = shader;
		this.buffers = buffers;
		this.target = null;

		this.drawMode = 'TRIANGLES';
		this.vertexCount = 0;
		this.indexCount = 0;

		this.uniforms = {};
		this.indexSubarrays = [];

		this.STATIC = STATIC || false;
		this.cached = false;

		this.glEnable = glEnable || [];

		this.blend = {
			src: 'SRC_ALPHA',
			dst: 'ONE_MINUS_SRC_ALPHA'
		};

		this.cIndexedUniforms = {};
	}
	draw(gl) {
		if (!this.cached) {
			for (let i = 0; i < this.length; i++) {
				if (this[i] && this[i].prepare) {
					this[i].prepare(gl);
				}
			}
		}

		for (let feature of this.glEnable) gl.enable(gl[feature]);
		gl.blendFunc(gl[this.blend.src], gl[this.blend.dst]);

		if (!this.shader.use(gl)) {
			this.gllog += '// !!! shader not binded\n';
			return;
		}

		for (let i = 0; i < this.length; i++) {
			if (this[i]) {
				if (!this[i].bind(gl, this.shader, i)) {
					this.gllog += '// !!! texture #' + i + ' not binded\n';
					return;
				}
			}
		}

		this.alignIndexes();

		for (let name in this.buffers) {
			this.buffers[name].bind(gl, this.shader, name);
			if (!this.cached) {
				this.buffers[name].sendData(gl);
			}
		}

		for (let i in this.uniforms) {
			this.gllog += '/* ' + i + ' */ ';
			this.shader.setUniform(gl, i, this.uniforms[i]);
		}

		if (this.buffers.index) gl.drawElements(gl[this.drawMode], this.indexCount, gl[this.buffers.index.type], 0);
		else gl.drawArrays(gl[this.drawMode], 0, this.vertexCount);

		this.cached = this.STATIC;

		for (let feature of this.glEnable) gl.disable(gl[feature]);
	}
	alignIndexes() {
		if (this.buffers.index) {
			for (let i = 0; i < this.indexSubarrays.length; i += 3) {
				let pos = this.indexSubarrays[i],
					size = this.indexSubarrays[i + 1],
					shift = this.indexSubarrays[i + 2],
					noIndex = 0;

				for (let j = pos; j < pos + size; j++) {
					if (!this.buffers.index.data[j]) {
						noIndex++;
					}
					this.buffers.index.data[j] += shift;
				}
				if (noIndex === size) {
					console.error('all indexes are zero', this);
				}
			}
			this.indexSubarrays.length = 0;
		}
	}
	prepare(gl) {
		if (!this.cached) {
			if (!this.target) {
				this.target = new Target();
			}
			this.target.beginRenderTo(gl);
			this.draw(gl);
			this.target.endRenderTo(gl);
		}
	}
	bind(gl, shader, i) {
		this.target.bind(gl, shader, i);
	}
	allocSubbuffers(verticiesCount, indicesCount) {
		this.gllog += 'allocSubbuffers (' + verticiesCount + ', ' + indicesCount + ')';
		let buffers = {};

		for (let name in this.buffers) {
			if (name !== 'index') {
				buffers[name] = this.buffers[name].alloc(this.vertexCount, verticiesCount);
			}
		}

		if (indicesCount && this.buffers.index) {
			let index = this.buffers.index;
			buffers.index = index.alloc(this.indexCount, indicesCount);

			this.indexSubarrays.push(this.indexCount);
			this.indexSubarrays.push(indicesCount);
			this.indexSubarrays.push(this.vertexCount);

			this.indexCount += indicesCount;
		}

		this.vertexCount += verticiesCount;
		this.cached = false;

		return buffers;
	}
	clear() {
		this.vertexCount = 0;
		this.indexCount = 0;
		this.indexSubarrays.length = 0;
		this.cached = false;
		for (let name in this.uniforms) {
			if (name.indexOf('[') !== -1) {
				delete this.uniforms[name];
			}
		}
	}
	getIndexedUniformIndex(name, value) {
		if (!value) debugger;
		
		for (let i = 0; i < 1000; i++) {
			let uniformArrayName = name + 's[' + i + ']';
			if (this.uniforms[uniformArrayName] === undefined) {
				this.uniforms[uniformArrayName] = value;
				return i;
			}
			if (this.uniforms[uniformArrayName].toString() === value.toString()) {
				return i;
			}
		}
	}
	getVertex(i) {
		let buffers = {};

		for (let name in this.buffers) {
			if (name !== 'index') {
				buffers[name] = this.buffers[name].data.subarray(i * this.buffers[name].cpv, (i + 1) * this.buffers[name].cpv);
			}
		}
		return buffers;
	}
}

class Canvas extends Array {
	constructor(canvas, orientation = 'none', update, onFrame, gl, prevUpdate, nextFrame, PIXEL, paused, vmin, vw, vh, width, height, OFFSET, RESOLUTION, batch) {
		super();

		gl = canvas.getContext('webgl');
		gl.enable(gl.BLEND);
		gl.enable(gl.CULL_FACE);
		gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
		gl.disable(gl.DEPTH_TEST);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.slv = '';

		if (window.WebGLDebugUtils) {
			WebGLDebugUtils.init(gl, {alpha:true});
			gl = WebGLDebugUtils.makeDebugContext(gl,
				(err, func, args) => {
					if (batch) {
						batch.gllog += `^^^ERROR ${WebGLDebugUtils.glEnumToString(err)}\n`;
						console.error(batch.gllog);
					}	
				},
				(func, args) => {
					if (batch) {
						batch.gllog += `${func}(${WebGLDebugUtils.glFunctionArgsToString(func, args)})\n`;
					}
				}
			);
		}

		gl.resetViewport = (w, h) => {
			if (w >= 0 && h >= 0) {
			} else {
				w = canvas.offsetWidth;
				h = canvas.offsetHeight;
			}
			if ((canvas.width !== w || canvas.height !== h) && orientation !== 'none') {
				canvas.width = w * 0.75;
				canvas.height = h * 0.75;
			}
			w = RESOLUTION[0] = canvas.width;
			h = RESOLUTION[1] = canvas.height;
			if (h > w) {
				vmin = w / h;
				vw = vmin;
				vh = 1;
				height = 1;
				width = vmin;
				gl.viewport(-h, -h, h * 2, h * 2);
				PIXEL = 1 / h;
			} else {
				vmin = h / w;
				vw = 1;
				vh = vmin;
				width = 1;
				height = vmin;
				gl.viewport(-w, -w, w * 2, w * 2);
				PIXEL = 1 / w;
			}
		};

		canvas.addEventListener('resize', gl.resetViewport);
		window.addEventListener('resize', gl.resetViewport);
		window.addEventListener('keydown', (e) => {
			if (e.altKey || e.ctrlKey) return;
			nextFrame = paused;
			paused = e.keyCode == 13;
		});
		canvas.style['user-select'] = 'none';

		if (window.Touches) {
			new Touches({
				element: document.querySelector('canvas'),
				ondrag: (id, x, y) => {
					for (let batch of this) {
						if (batch.ondrag) {
							batch.ondrag(id, x * width - OFFSET[0], y * height - OFFSET[1]);
						}
					}
				},
				ontouch: (id, x, y) => {
					for (let batch of this) {
						if (batch.ontouch) {
							batch.ontouch(id, x * width - OFFSET[0], y * height - OFFSET[1]);
						}
					}
				}
			});
		}

		RESOLUTION = [0, 0];
		OFFSET = [0, 0];
		gl.resetViewport();

		onFrame = (timestamp) => {
			prevUpdate = prevUpdate || timestamp;
			let dt = (timestamp - prevUpdate) * 0.001;
			if (paused) dt = 0;
			if (dt > 0.05 || nextFrame) dt = 0.02;

			if (dt) {
				for (let batch of this) {
					if (batch) {
						if (!batch.STATIC) {
							batch.clear();
						}
					}
				}

				OFFSET = update(dt, vmin, () => paused = nextFrame = true, vw, vh) || [0,0];
				OFFSET[0] += width / 2;
				OFFSET[1] += height / 2;
			}

			for (batch of this) {
				if (batch) {
					batch.gllog = '';
					batch.uniforms['PIXEL'] = PIXEL;
					batch.uniforms['TIME'] = timestamp % 1000;
					batch.uniforms['OFFSET'] = OFFSET;
					batch.uniforms['RESOLUTION'] = RESOLUTION;
					batch.uniforms['RANDOM'] = Math.random();
					batch.draw(gl, dt);
				}
			}

			for (batch of this) {
				if (batch.tracegl) {
					console.log(batch.gllog);
					batch.tracegl = false;
				}
			}

			prevUpdate = timestamp;
			nextFrame = false;

			requestAnimationFrame(onFrame, canvas);
		};

		requestAnimationFrame(onFrame, canvas);
	}
}

class Canvas3D extends Array {
	constructor(canvas, update, resizeCb, onFrame, gl, prevUpdate, vmin, width, height, batch) {
		super();

		gl = canvas.getContext('webgl2', {alpha: false});
		gl.slv = '#version 300 es\n';

		if (window.WebGLDebugUtils) {
			WebGLDebugUtils.init(gl);
			gl = WebGLDebugUtils.makeDebugContext(gl,
				(err, func, args) => {
					if (batch) {
						batch.gllog += `^^^ERROR ${WebGLDebugUtils.glEnumToString(err)}\n`;
						console.error(batch.gllog);
						debugger;
					}	
				},
				(func, args) => {
					if (batch) {
						batch.gllog += `${func}(${WebGLDebugUtils.glFunctionArgsToString(func, args)})\n`;
					}
				}
			);
		}

		let RESOLUTION = [0, 0];

		gl.resetViewport = (w, h) => {
			w = w || canvas.offsetWidth,
			h = h || canvas.offsetHeight;
			//w/=2;h/=2;
				canvas.width = w;
				canvas.height = h;
			gl.viewport(0, 0, w, h);
			resizeCb(w, h);
			RESOLUTION[0] = w;
			RESOLUTION[1] = h;
		};

		window.addEventListener('resize', () => gl.resetViewport());
		canvas.addEventListener('fullscreenchange', () => gl.resetViewport());
		canvas.style['user-select'] = 'none';

		gl.resetViewport();

		onFrame = (timestamp) => {
			prevUpdate = prevUpdate || timestamp;
			let dt = (timestamp - prevUpdate) * 0.001;

			if (dt) {
				for (let batch of this) {
					if (batch) {
						if (!batch.STATIC) {
							batch.clear();
						}
					}
				}

				update(dt, vmin);
			}

			for (batch of this) {
				if (batch) {
					batch.gllog = '';
					batch.uniforms.TIME = timestamp;
					batch.uniforms.RESOLUTION = RESOLUTION;
					batch.draw(gl, dt);
				}
			}

			for (batch of this) {
				if (batch.tracegl) {
					console.log(batch.gllog);
					batch.tracegl = false;
				}
			}

			prevUpdate = timestamp;

			requestAnimationFrame(onFrame, canvas);
		};

		requestAnimationFrame(onFrame, canvas);
	}
}

let makeBuffer = {
	quadIndexes(buf) {
		buf[0] = 0;
		buf[1] = 1;
		buf[2] = 2;
		buf[3] = 2;
		buf[4] = 3;
		buf[5] = 0;
	},
	quad(buf, x0, y0, x1, y1) {
		buf[0] = x0;	buf[1] = y0;
		buf[2] = x1;	buf[3] = y0;
		buf[4] = x1;	buf[5] = y1;
		buf[6] = x0;	buf[7] = y1;
	},
	quadByCenter(buf, x, y, w, h) {
		w *= 0.5;
		h *= 0.5;
		buf[0] = x - w;	buf[1] = y - h;
		buf[2] = x + w;	buf[3] = y - h;
		buf[4] = x + w;	buf[5] = y + h;
		buf[6] = x - w;	buf[7] = y + h;
	},
	subrect(buf, srcx1, srcy1, srcx2, srcy2, dstx1, dsty1, dstx2, dsty2) {
		let map = (src, dst0, dst1) => dst0 + src / (dst1 - dst0);
		makeBuffer.quad(
			buf,
			map(srcx1, dstx1, dstx2),
			map(srcy1, dsty1, dsty2),
			map(srcx2, dstx1, dstx2),
			map(srcy2, dsty1, dsty2)
		);
	},
	repeat(buf, arr) {
		for (let i = 0; i < buf.length; i += arr.length) {
			for (let j = 0; j < arr.length; j++) {
				buf[i + j] = arr[j];
			}
		}
	}
};

let math2d = {
	move(buf, x, y) {
		for (let i = 0; i < buf.length; i += 2) {
			buf[i] += x;
			buf[i + 1] += y;
		}
	},
	scale(buf, scalex, scaley) {
		if (typeof(scaley) !== 'number') scaley = scalex;
		for (let i = 0; i < buf.length; i += 2) {
			buf[i] = buf[i] * scalex;
			buf[i + 1] = buf[i + 1] * scaley;
		}
	},
	rotate(buf, rad) {
		let si = Math.sin(rad);
		let co = Math.cos(rad);
		for (let i = 0; i < buf.length; i += 2) {
			let x = buf[i];
			let y = buf[i + 1];
			buf[i] = x * co - y * si;
			buf[i + 1] = x * si + y * co;
		}
	},
	matrix(dx, dy, a = 0, sx = 1, sy = 1) {
		let co = 1,
			si = 0;
		if (a !== 0) {
			co = Math.cos(a);
			si = Math.sin(a);
		}
		let mat3 = [
			co * sx, si, 0,
			-si, co * sy, 0,
			dx, dy, 0
		];
		return mat3;
		
		/*c11 = x · b11 + y · b21 + b31 = x · 11 + y · 21 + 31 = 11 + 42 + 93 = 146
		c12 = x · b12 + y · b22 + b32 = x · 12 + y · 22 + 32 = 12 + 44 + 96 = 152
		c13 = x · b13 + y · b23 + b33 = x · 13 + y · 23 + 33 = 13 + 46 + 99 = 158

		c11 = x · co * sx + y · -si + dx = 11 + 42 + 93 = 146
		c12 = x · si + y · co * sy + dy = 12 + 44 + 96 = 152
		c13 = x · 13 + y · 23 + 33 = 13 + 46 + 99 = 158*/
	}
};

function genTileUV(mask, mirror, n, count) {
	// 3 2
	// 0 1
	const tileStart = {
		'--++': [ 1, 0.2 ],
		'++--': [ 1, 0.8 ],
		'+---': [ 0.5, 0.8 ],
		'+--+': [ 0.5, 0.6 ],
		'---+': [ 0.5, 0.2 ],
		'+++-': [ 1, 0.4 ],
		'-+++': [ 1, 0.6 ]
	};

	let s = tileStart[mask],
		xadd1 = mirror ? 0.5 : 0,
		xadd2 = mirror ? 0 : 0.5,
		uv = [];

	if (s) {
		uv = [
			s[0] - xadd1, s[1],
			s[0] - xadd2, s[1],
			s[0] - xadd2, s[1] + 0.2,
			s[0] - xadd1, s[1] + 0.2
		];
	} else if (mask === '++++') {
		uv = [0.6, 0.05, 0.8, 0.05, 0.8, 0.15, 0.6, 0.15];
	} else if (mask === '-+-+') {
		uv = [0.1, 0.05, 0.3, 0.05, 0.3, 0.15, 0.1, 0.15];
	}

	for (let i = 0; i < uv.length; i++) {
		if (i % 2) {
			uv[i] = 1 - uv[i];
		} else {
			if (uv[i] === 0) uv[i] = 0.05;
			if (uv[i] === 1) uv[i] = 0.9;
			uv[i] /= count;
			uv[i] += n / count;
		}
	}

	return uv;
}

window.tinyglhelper = {
	Buffer,
	Sampler,
	ArrayTexture,
	Shader,
	Target,
	Batch,
	Canvas,
	Canvas3D,
	makeBuffer,
	math2d,
	autocreateBatch(fragSrc, verticiesCount = 0, vertSrc, drawMode = 'TRIANGLES') {
		let buffers = {
			position: new Buffer('FLOAT', 2, 'ARRAY_BUFFER', null, verticiesCount, 'position')
		};

		if (drawMode === 'TRIANGLES' || drawMode === 'LINES') {
			buffers.index = new Buffer('UNSIGNED_SHORT', 1, 'ELEMENT_ARRAY_BUFFER', null, verticiesCount * 2, 'index');
		}
		
		if (vertSrc) {
			let attributes = parseShader(vertSrc, 'attribute');

			for (let a in attributes) {
				let type = attributes[a].type,
					qual = attributes[a].qual;
				buffers[a] = new Buffer(BUFFER_TYPE[qual], BUFFER_CPV[type], 'ARRAY_BUFFER', null, verticiesCount, a);
			}
		} else { // lets gen vert src in Shader::Update(); varyings can start with "v_"
			let varyings = parseShader(fragSrc, 'varying');

			for (let v in varyings) {
				let type = varyings[v].type,
					qual = varyings[v].qual;
				buffers[v.substr(2)] = new Buffer(BUFFER_TYPE[qual], BUFFER_CPV[type], 'ARRAY_BUFFER', null, verticiesCount, v);
			}
		}

		const isStatic = verticiesCount > 0;
		let batch = new Batch(buffers, new tinyglhelper.Shader(fragSrc, vertSrc), isStatic);
		batch.drawMode = drawMode;
		return batch;
	},
	tilemap(batch, map, id, n = 0, count = 1) {
		let tilesMade = 0;
		const tileMirror = {
			'-+--': '+---',
			'-++-': '+--+',
			'--+-': '---+',
			'++-+': '+++-',
			'+-++': '-+++',
			'+-+-': '-+-+'
		};
		for (let y = 0.5; y < map.length - 0.5; y++) {
			for (let x = 0.5; x < map[0].length - 0.5; x++) {
				let x0 = (x - 0.49) | 0,
					y0 = (y - 0.49) | 0,
					x1 = (x + 0.51) | 0,
					y1 = (y + 0.51) | 0,
					mask = '',
					mirror = false;

				mask += map[y0][x0] === id ? '+' : '-';
				mask += map[y0][x1] === id ? '+' : '-';
				mask += map[y1][x1] === id ? '+' : '-';
				mask += map[y1][x0] === id ? '+' : '-';

				if (mask !== '----') {
					if (tileMirror[mask]) {
						mask = tileMirror[mask];
						mirror = true;
					}

					let buffers = batch.allocSubbuffers(4, 6);
					makeBuffer.quad(buffers.position, x0, y0, x1, y1);
					buffers.index.set([0, 1, 2, 0, 2, 3]);
					buffers.uv.set(genTileUV(mask, mirror, n, count));

					tilesMade++;
				}
			}
		}
		return tilesMade;
	},
	drawSprite(batch, x = 0, y = 0, w = 1, h = 1, uvs = [0, 0, 1, 1]) {
		h = h || w;
		let bufs = batch.allocSubbuffers(4, 6);
		bufs.index.set([0, 1, 2, 0, 2, 3]);
		if (bufs['uv']) makeBuffer.quad(bufs['uv'], ...uvs);
		makeBuffer.quad(bufs.position, x, y, x + w, y + h);
		return bufs;
	},
	drawSpriteCentered(batch, x = 0, y = 0, w = 1, h = 1, uvs = [0, 0, 1, 1], defVal) {
		return tinyglhelper.drawSprite(batch, x - w / 2, y - h / 2, w, h, uvs);
	},
	drawSpriteBuffers(batch, bufs) {
		let b = batch.allocSubbuffers(4, 6);
		for (let i in bufs) {
			if (b[i] && bufs[i]) {
				b[i].set(bufs[i]);
			}
		}
	},
	async loadAll(srcArr, lib, convertFns, onprogress) {
		let promiseArr = [],
			progress = 0;
		for (let src of srcArr) {
			let res = fetch(src).then(async res => {
				let ext = src.split('.').pop();
				if (!convertFns[ext]) debugger;
				lib[src] = await convertFns[ext](res);
				onprogress(++progress / srcArr.length);
			});
			promiseArr.push(res);
		}
		return Promise.all(promiseArr);
	},
	uvFromGrid(gridWidth, gridHeight, index) {
		let col = index % gridWidth,
			row = (index / gridWidth) | 0;
		return [
			col / gridWidth,
			1 - (row + 1) / gridHeight,
			(col + 1) / gridWidth,
			1 - row / gridHeight
		];
	}
};